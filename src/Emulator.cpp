#include "Emulator.hpp"

#include <chrono>
#include <functional>
#include <iostream>

Emulator::LaserState::LaserState() : emissionOn(false), laserPower(0)
{
}

Emulator::Emulator() : mTimer(mService)
{
}

Emulator::~Emulator()
{
  Stop();
}

void Emulator::Start()
{
  mWork = std::make_unique<asio::io_service::work>(asio::io_service::work(mService));
  mWorkerThread = std::thread([this](){this->mService.run();});
}

void Emulator::Stop()
{
  mWork.reset();
  mWorkerThread.join();
  mService.stop();
}

void Emulator::ExecuteInstruction(Instruction aInstruction)
{
  switch(aInstruction.GetOp())
  {
    case EOp::STR:
      mService.post(std::bind(&Emulator::HandleSTR, this));
      break;
    case EOp::STP:
      mService.post(std::bind(&Emulator::HandleSTP, this));
      break;
    case EOp::STQ:
      mService.post(std::bind(&Emulator::HandleSTQ, this));
      break;
    case EOp::KAL:
      mService.post(std::bind(&Emulator::HandleKAL, this));
      break;
    case EOp::PWQ:
      mService.post(std::bind(&Emulator::HandlePWQ, this));
      break;
    case EOp::PWE:
      if(auto power = aInstruction.GetAttribute())
      {
        mService.post(std::bind(&Emulator::HandlePWE, this, *power));
      }
      else
      {
        assert(false);
      }
      break;
    default:
      return;
  }
}

void Emulator::HandleSTR()
{
  std::cout<<"STR#"<<std::endl;
  mState.emissionOn = true;
  SetLaserTimer();
}

void Emulator::HandleSTP()
{
  std::cout<<"STP#"<<std::endl;
  SetLaserOff();
}

void Emulator::HandleSTQ()
{
  std::cout<<"ST?|"<<mState.emissionOn<<"#"<<std::endl;
}

void Emulator::HandleKAL()
{
  std::cout<<"KAL#"<<std::endl;
  SetLaserTimer();
}

void Emulator::HandlePWQ()
{
  std::cout<<"PW?|"<<mState.laserPower<<"#"<<std::endl;
}

void Emulator::HandlePWE(int aPower)
{
  if(mState.emissionOn)
  {
    std::cout<<"PW=#"<<std::endl;
    mState.laserPower = aPower;
  }
  else
  {
    std::cout<<"PW=!"<<std::endl;
  }
}

void Emulator::SetLaserTimer()
{
  mTimer.expires_from_now(std::chrono::seconds(5));
  mTimer.async_wait([this](const asio::error_code& e){
      if(e){ return; }
      SetLaserOff();
  });
}

void Emulator::SetLaserOff()
{
  mState.emissionOn = false;
  mState.laserPower = 0;
}
