#ifndef IEMULATOR_HPP
#define IEMULATOR_HPP

#include "Instruction.hpp"

/** 
    @brief Emulator interface 
*/  
class IEmulator
{
public:
  /** 
    @brief Starts the emulator.
    @return void
  */  
  virtual void Start() = 0;

  /** 
    @brief Stops the emulator.
    @return void
  */  
  virtual void Stop() = 0;

  /*! Post an instruction to be executed by the emulator.
    @param aInstruction Instruction to be executed.
    @return void
  */  
  virtual void ExecuteInstruction(Instruction aInstruction) = 0;
};

#endif // IEMULATOR_HPP
