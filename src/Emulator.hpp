#ifndef EMULATOR_HPP
#define EMULATOR_HPP

#include "IEmulator.hpp"

#include <asio.hpp>
#include <memory>
#include <optional>

class Emulator : public IEmulator
{
public:
  Emulator();
  virtual ~Emulator();

  void Start() override;
  void Stop() override;
  void ExecuteInstruction(Instruction aInstruction) override;

private:
  void HandleSTR();
  void HandleSTP();
  void HandleSTQ();
  void HandleKAL();
  void HandlePWQ();
  void HandlePWE(int aPower);

  void SetLaserTimer();
  void SetLaserOff();

private:
  struct LaserState
  {
    LaserState();

    bool emissionOn;
    int laserPower;
  };

  LaserState mState;
  asio::io_service mService;
  asio::steady_timer mTimer;
  std::unique_ptr<asio::io_service::work> mWork;
  std::thread mWorkerThread;
};

#endif // EMULATOR_HPP
