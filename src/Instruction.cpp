#include "Instruction.hpp"

Instruction::Instruction(EOp aOp, const std::optional<int>& aAttribute) : mOp(aOp), mAttribute(aAttribute)
{
}

EOp Instruction::GetOp()
{
  return mOp;
}

std::optional<int> Instruction::GetAttribute()
{
  return mAttribute;
}
