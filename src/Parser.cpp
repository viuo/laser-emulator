#include "Parser.hpp"

#include "Emulator.hpp"

#include <iostream>
#include <sstream>
#include <vector>

Parser::Parser() : mSillyMode(false)
{
}

Instruction Parser::ParseCommand(const std::string& aCommand, const std::string& aAttribute)
{
  if(aCommand == "STR")
  {
    return Instruction(EOp::STR);
  }
  else if(aCommand == "STP")
  {
    return Instruction(EOp::STP);
  }
  else if(aCommand == "ST?")
  {
    return Instruction(EOp::STQ);
  }
  else if(aCommand == "KAL")
  {
    return Instruction(EOp::KAL);
  }
  else if(aCommand == "PW?")
  {
    return Instruction(EOp::PWQ);
  }
  else if(aCommand == "PW=")
  {
    try
    {
      int value = std::stoi(aAttribute);
      if(value < 100 || value > 0)
      {
        return Instruction(EOp::PWE, value);
      }
      else
      {
        std::cout<<"PW!"<<std::endl;
        return Instruction(EOp::UK);
      }
    }
    catch(std::exception e)
    {
        std::cout<<"PW!"<<std::endl;
        return Instruction(EOp::UK);
    }
  }
  else if(aCommand == "ESM")
  {
    mSillyMode = true;
    std::cout<<"ESM#"<<std::endl;
    return Instruction(EOp::ESM);
  }
  else if(aCommand == "DSM")
  {
    mSillyMode = false;
    std::cout<<"DSM#"<<std::endl;
    return Instruction(EOp::DSM);
  }

  std::cout<<"UK!"<<std::endl;
  return Instruction(EOp::UK);
}

Instruction Parser::ParseLine()
{
  std::string line;
  std::getline(std::cin, line);
  
  std::pair<std::string, std::string> split;
  if(mSillyMode)
  {
    split = SplitCommand(line.rbegin(), line.rend());
  }
  else
  {
    split = SplitCommand(line.begin(), line.end());
  }

  return ParseCommand(split.first, split.second);
}
