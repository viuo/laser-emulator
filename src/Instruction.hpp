#ifndef INSTRUCTION_HPP
#define INSTRUCTION_HPP

#include <optional>

enum class EOp
{
 STR, // STR - Starts emission of the laser. Only allowed if emission is currently off.
 STP, // STP - Stops emission of the laser. Returns an error if emission is currently off.
 STQ, // ST? - Returns the emission status of the laser. Returns a single value, 
      //       which is 0 for emission off, and 1 for emission on.
 KAL, // KAL - Keep alive signal. If the laser is emitting, but hasn’t received any KAL 
      //       keep-alive signals for longer than 5 seconds, the laser automatically turns 
      //       off emission.
 PWQ, // PW? - Returns the current laser power. If the laser is not currently emitting, returns 0. 
      //       Otherwise returns an integer in the range 1–100.
 PWE, // PW= - Sets the current laser power. Only allowed if the laser is emitting.
 ESM, // ESM - Sets the controller into ‘silly mode’, in which it interprets all incoming command 
      //       strings backwards. E.g., in silly mode, one has to send the command string PTS in 
      //       order to stop emission. Responses are not affected.
 DSM, // DSM - Disables ‘silly mode’.
 UK   // UK  - Unknown instruction.
};

/** 
  @brief Memory representation of an emulator instruction.
*/  
class Instruction
{
public:
  /** 
    @brief Constructor for Instruction
    @param aOp The type of operation to be performed.
    @param aAttribute Optional parameter to the operation.
    @return The constructed Instruction.
  */  
  Instruction(EOp aOp, const std::optional<int>& aAttribute = std::nullopt);

  /** 
    @brief Returns the type of operation.
    @return The type of operation.
  */  
  EOp GetOp();

  /** 
    @brief Returns the optional attribute.
    @return The attribute.
  */  
  std::optional<int> GetAttribute();

private:
  EOp mOp;
  std::optional<int> mAttribute;
};

#endif // INSTRUCTION_HPP
