#ifndef PARSER_HPP
#define PARSER_HPP

#include <string>

class Instruction;

/** 
  @brief Parser for the console input to emulator instructions.
*/  
class Parser
{
public:
  Parser();

  /** 
    @brief Read line from console and parse it into an instruction.
    @return The parsed instruction.
  */  
  Instruction ParseLine();

private:
  Instruction ParseCommand(const std::string& aCommand, const std::string& aAttribute);

  template <typename Iterator>
  std::pair<std::string, std::string> SplitCommand(Iterator aBegin, Iterator aEnd)
  {
    auto it = aBegin;

    std::string operation;
    while(it != aEnd && *it != '|')
    {
      operation.push_back(*it);
      ++it;
    }

    if(it != aEnd && *it == '|')
    {
      ++it;
    }

    std::string argument;
    while(it != aEnd)
    {
      argument.push_back(*it);
      ++it;
    }
    return {operation, argument};
  }

private:
  bool mSillyMode;
};

#endif // PARSER_HPP
