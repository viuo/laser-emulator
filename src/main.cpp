#include "Emulator.hpp"
#include "Parser.hpp"

int main()
{
  Parser parser;
  Emulator emulator;
  emulator.Start();
  while(true)
  {
    emulator.ExecuteInstruction(parser.ParseLine());
  }
}
