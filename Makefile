SRCS := $(wildcard src/*.cpp)
OBJS = $(SRCS:.cpp=.o)
CXXFLAGS= -I lib -DASIO_STANDALONE -std=c++17
APP=laser

all: ${APP}

${APP}: $(OBJS)
	clang++ ${OBJS} -lpthread -o ${APP}

.cpp.o:
	clang++ ${CXXFLAGS} -c $< -o $@

clean:
	rm laser src/*.o
